--x 1. Retornar os nomes e a data de nascimento de todos os velejadores.
select sname, dbirth, 1
    from sailor
;

--x 2. Retornar os nomes e a descrição da classe de todos os velejadores.
select sname, ldsc, 2
    from sailor
    join tplevel
    on slevel = lid
;

--x 3. Retornar os nomes e as potencias dos barcos de mais de 2 velas.
select bname, bhorses, 3
    from boat
    where bnsail > 2
;

--x 4. Retornar dados dos velejadores e barcos que tiveram duração das reservas superiores a 20 horas.
    select sname, semail, dbirth, ldsc, dtimep, dtimer, bname, blen, bhorses, byear, bnsail, tbdsc, 4
    from reserve r
    join sailor s
    on s.sid = r.sid
        join boat b
        on r.bid = b.bid
            join tpboat tb
            on b.tbid = tb.tbid
                join tplevel tl
                on s.slevel = tl.lid
    where (r.dtimer - r.dtimep) > '20:00'
;

--x 5. Retornar os dados dos barcos azuis cuja a potência seja superior a 30 HP.
select bname, blen, bhorses, byear, bweight, bnsail, 5
    from boat
    natural join tpboat
    where tbdsc in ('catboat', 'Catboat') and bhorses > 30
;

--x 6. Retornar os dados das reservas (apresentar os nomes ao invés dos códigos) feitas por velejadores iniciantes (Junior) de barcos do tipo (Laser).
select sname, ldsc, bname, tbdsc, dtimep, dtimer,  6
    from reserve r
    join sailor s
    on r.sid = s.sid
        join boat b
        on r.bid = b.bid
            join tpboat tb
            on tb.tbid = b.tbid
                join tplevel tl
                on tl.lid = s.slevel
    where b.tbid = 20 and s.slevel in (20, 1)
;

--x 7. Retornar o nome dos velejadores com mais de quarenta anos (divida por 365 e utilize current_date).
select sname, (current_date - dbirth)/365 as idade, 7
    from sailor
    where (current_date - dbirth) / 365 > 40
;

--x 8. Retornar os dados dos barcos cujos os tamanhos sejam inferiores a 10 feet (pés).
select bname, blen, bhorses, byear, bweight, bnsail, 8
    from boat b
    where b.blen < 10
;

--x 9. Retornar a quantidade de reservas realizadas em 03/07/2006.
--/sum(1) também dá certo
select count(*), 9
    from reserve
    where dtimep > '03/07/2006'
;

--x 10. Retornar a média de potência dos barcos Scooner.
select avg(bhorses), 10
    from boat
        natural join tpboat
    where tbdsc = 'Schooner'
;

--x 11. Retornar a maior potência dos barcos por tipo.
select tbdsc, max(bhorses), 11
    from boat
    natural join tpboat
    group by tbdsc
;

--x 12. Retornar a quantidade de velejadores por classe. Deve aparecer a descrição da classe e não o código.
select ldsc, count(sid), 12
    from tplevel
    left join sailor
    on slevel = lid
    group by lid
;

--x 13. Retornar os dados de todos os barcos e de suas respectivas reservas, caso existirem.
select bname, blen, bhorses, byear, bweight, bnsail, dtimep, dtimer, dtimer-dtimep as "Tempo emprestado", 13
    from boat
    natural left join reserve
    order by bname, dtimep
;
--x 14. Retornar os nomes dos velejadores e dos barcos que ainda não foram devolvidos.
select sname, bname, 14
    from sailor
    natural join reserve
        natural join boat
    where dtimer is NULL
;

--x 15. Retornar o nome do velejador e do barco que tiveram o maior tempo de reserva (podem existir mais de um).
select sname, bname, dtimer-dtimep as "Tempo de reserva", 15
    from sailor
    natural join reserve
        natural join boat
    where dtimer is not null and dtimer - dtimep = (select max(dtimer-dtimep) from reserve where dtimer is not null)
    order by dtimer - dtimep desc
;

--x 16. Retornar os dados dos barcos que nunca foram reservados.
select bname, blen, bhorses, byear, bweight, bnsail, 16
    from boat
    natural left join reserve
    where dtimep is null
;

--x 17. Retornar o nome de todos os velejadores com as suas reservas caso existam.
select sname, bname as "Reserved Boat", dtimer - dtimep as "Tempo de reserva", 17
    from sailor
    natural left join reserve
        natural join boat
    order by sname asc
;

--x 18. Retornar o nome e classe (descrição) dos velejadores que fizeram menos de 10 reservas.
select sname, ldsc, 18
    from sailor s
    natural left join reserve r
        join tplevel on slevel = lid
    where (select count(*) from reserve where s.sid = r.sid) < 10
    group by sname, ldsc
;

select sname, ldsc, count(r.sid) as "Nº. reservas", 18.2
    from sailor s
        natural left join reserve r
        join tplevel tl on s.slevel = tl.lid
    group by sname, ldsc1
    having count(r.sid) < 10
;

--x 19. Retornar os nomes dos barcos com seus respectivos totais de tempo de reserva.
select bname, sum(r.dtimer - r.dtimep) as "Tempo reservado", 19
    from boat b
        natural left join reserve r
    group by bname
;

--x 20. Retornar os nomes dos velejadores e barcos reservados por velejadores amadores no ano de 2007.
select sname, bname, ldsc, 20
    from reserve
        natural join sailor
        natural join boat
        natural join tplevel
    where ldsc like 'Amateur' and (dtimep between '01/01/2007' and '31/12/2007' or dtimer between '01/01/2007' and '31/12/2007')
;

--x 21. Retornar nome do velejador, descrição da classe do velejador, nome e tipo do barco das reservas feitas pelo velejador.
select sname, ldsc, bname as "Nome Barco", tbdsc as "Tipo Barco", 21
    from sailor
        join tplevel on lid = slevel
        natural join reserve
        natural join boat
        natural join tpboat
;


--x 22. Retornar os dados do maior barco (ou maiores).
select bname, blen, bhorses, byear, bweight, bnsail, 22
    from boat
    where blen = (select max(blen) from boat)
;

--x 23. Retornar os dados dos velejadores que já reservaram o maior barco (ou maiores).
select sname, semail, dbirth, ldsc, 23
    from sailor
    join tplevel on slevel = lid
    natural join reserve
    natural join boat
    where blen = (select max(blen) from boat)
;

--x 24. Retornar o velejador mais novo da classe Master (pode existir mais de um).
select sname, (current_date - dbirth)/365 as "Idade", 24
    from sailor s
        join tplevel tl on s.slevel = tl.lid
    where ldsc = 'Master' and current_date - dbirth = (
                                                        select min(current_date-dbirth)
                                                            from sailor join tplevel on slevel = lid
                                                            where ldsc = 'Master'
                                                        )
;

-- 25. Retornar o nome do velejador (ou velejadores) que reservaram todos os barcos Laser cadastrados
select sname, count(distinct bid), 25
    from sailor
    natural join reserve
        natural join boat
        natural join tpboat
    where tbdsc = 'Laser'
    group by sname
    having count(distinct bid) = (
                                    select count(bid)
                                    from boat
                                    natural join tpboat
                                    where tbdsc = 'Laser'
                                )
;


-- Retornar o nome do velejador que fez o maior número de reservas (Pode ser mais de um)
