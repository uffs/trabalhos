//Esse fala de como eles usaram um algoritmo diferente e avaliado melhor que o FOF (Friends of Friends) para sugerirem amizades numa rede social (Oro-aro).
@inproceedings{agbfrsuga,
  title={A graph-based friend recommendation system using genetic algorithm},
  author={Silva, Nitai B and Tsang, Ren and Cavalcanti, George DC and Tsang, Jyh},
  booktitle={IEEE Congress on Evolutionary Computation},
  pages={1--7},
  year={2010},
  organization={IEEE}
}

//esse fala das recomendações de amigos usando grupos de e-mail
@inproceedings{roth2010suggesting,
  title={Suggesting friends using the implicit social graph},
  author={Roth, Maayan and Ben-David, Assaf and Deutscher, David and Flysher, Guy and Horn, Ilan and Leichtberg, Ari and Leiser, Naty and Matias, Yossi and Merom, Ron},
  booktitle={Proceedings of the 16th ACM SIGKDD international conference on Knowledge discovery and data mining},
  pages={233--242},
  year={2010},
  organization={ACM}
}
Aplicações do grafos nas recomendações de amizades das redes sociais
Possível alternativa: A abstração de grafos nas recomendações online
Possível alternativa: Uso de grafos nos sistemas de recomendações de serviços online

• As relações sociais podem ser vistas como um grafo
• As relações de hoje até melhor são vistas como um grafo
• as pessoas não os nós
• as relações são as arestas
• as interações podem ser ligações
• arquivos, fotos, vídeos, grupos, etc podem ser vistos como nós e ligações
• artigo fala sobre recomendar amigos baseados no grafo implícito
• cita e explica algumas aplicações de grafos em recomendações


\cite{} //falar do "amigos de amigos " (friends of friends, FOF)

Falar sobre passeios aleatórios antes de falar de alguns algoritmos.

Diferente dos grafos explícitos, onde os usuários dizem quem são seus amigos enviando ou aceitando convites de amizades, o grafo implícito leva em consideração as interações entre os usuários e seus contatos ou grupos de contatos.
Nesta implementação, apenas as conexões que o usuário tem com outros vértices são consideradas. Isso é necessário para evitar que algum usuário tenha acesso a informações de relações de seus amigos que ele não teria acesso normalmente.

\cite{roth2010suggesting}
• considera não só interações entre usuários, mas também interações entre usuários e grupos
• análise grafo social implicito: rede social definida por interações entre usuários e seus contatos e grupos de contatos.
• diferença entre grafos implícitos e grafos explícitos (que os contatos explicitamente dizem quem são seus amigos adicionando-os à sua lista de contatos)
• o peso de suas arestas são determinadas por frequência, recência e direção de interações entre usuários e seus contatos ou grupos de contatos.
frequência: quão frequente é o contato com o grupo de usuários
recência: quão recente é o contato feito
direção: se o contato foi feito de modo passivo (onde o usuário só recebia e-mail), ativo (onde o usuário apenas envia e-mail) ou ambos (quando ele envia e recebe os e-mails).
• não são consideradas o conteúdo das interações, principalmente por serem privadas
• usa apenas a rede egocêntria do usuário, pra evitar mostrar pra este os contatos de seus amigos.
• não são considerados relações amigos de amigos
• dados uma rede de um usuário com arestas com peso e uma "semente" (grupo de amigos que o usuário já tem) inicial, o algoritmo expande essa semente
• mostra duas implementações desses algoritmos: "Don't forget Bob" e "Got the wrong Bob?"
Características da rede social implícita do Google Mail:
• composta por bilhões de nós
• mensagens entre usuários e grupos são consideradas como uma aresta apenas
• arestas nessa rede tem tanto peso como direção
Friend suggest algorithm
Aplicações:
• "Don't forget Bob": É um sistema que sugere, enquanto uma pessoa insere os destinatários de e-mail em uma mensagem que será enviada, outros possíves destinatários.
• "Got the wrong bob?": é um algoritmo que, para cada recipiente r, verifica se passando a lista L sem r como semente do Friend Suggest algorithm, r seria retornado como sugestão do grupo. Caso não seja, pode significar que o usuário que está escrevendo o e-mail tenha se confundido. O algoritmo então começa a procurar outros usuários na rede egocêntrica do escritor do e-mail que tenham maior afinidade com o grupo que está na lista de destinatários, segurindo-os caso encontre.
{talvez coloque a imagem que tem no artigo, talvez não}


@inproceedings{bogers2010movie,
  title={Movie recommendation using random walks over the contextual graph},
  author={Bogers, Toine},
  booktitle={Proc. of the 2nd Intl. Workshop on Context-Aware Recommender Systems},
  year={2010}
}

@inproceedings{yin2010linkrec,
  title={LINKREC: a unified framework for link recommendation with user attributes and graph structure},
  author={Yin, Zhijun and Gupta, Manish and Weninger, Tim and Han, Jiawei},
  booktitle={Proceedings of the 19th international conference on World wide web},
  pages={1211--1212},
  year={2010},
  organization={ACM}
}
• Relevância das recomendações:
(1)homophily: two persons who share more attributes are more likely to be linked than those who share fewer attributes;
(2)rarity: the rare attributes are likely to be more important, whereas the common attributes are less important;
(3)social influence: the attributes shared by a large percentage of friends of a particular person are important for predicting potential links for that person;
(4)common friendship: the more friends two persons share, the more likely they are to be linked together;
(5)social closeness: the potential friends are likely to be located close to each other in the socialgraph; and
(6)preferential attachment: a person is more likely to link to a popular person rather than to a person with fewfriends.

(1) weight all the attributes equally foreach person; (2) attach more weight to the more globally impor-tant attributes, where the global importance ofattribute measuresthe percentage of existing links among all the possible person pairswith that attribute; (3) attach more weight to the more locallyim-portant attributes,i.e.,the more the number of friends that share theattribute, the more important the attribute is for the person; and (4)mixglobal and local importance together by linear interpolation ormultiplication

(1)homophily: if two persons share more attributes,it is more likely to walk from one person to the other; (2)rarity: ifone attribute is rare, therearefewer outlinks for the correspondingattribute node, because the weight of each outlink is larger, so theprobability of a random walkoriginatingfrom a person via this at-tribute node is larger; (3)social influence: if one attribute is sharedby many of the existing linked personsof the given person, the ran-dom walk is more likely to pass through the existing linked personnodes to this attribute node; (4)common friendship: if two personsshare many friends, it is more likely to walk from one person to theother; (5)social closeness: if two persons are close to each otherin thegraph, the random walk probability from one to the other islikely to be larger than if they are far away from each other; and (6)preferentialattachment: if a person is very popular, there are manyinlinks to the person node in the graph, and for a random personnode in the graph,it is easier to access a node with more inlinks
• Co-authorship prediction is consideredas link recommendation problem for DBLP
•  Co-staring prediction is consid-ered as link recommendation problem for IMDB

@inproceedings{yan2012tweet,
  title={Tweet recommendation with graph co-ranking},
  author={Yan, Rui and Lapata, Mirella and Li, Xiaoming},
  booktitle={Proceedings of the 50th Annual Meeting of the Association for Computational Linguistics: Long Papers-Volume 1},
  pages={516--525},
  year={2012},
  organization={Association for Computational Linguistics}
}
• recomenda tweets para usuários
• três grafos: os tweets, os usuários e um outro bipartido ligando os dois
• random walks no grafos


\section{Abstract}
Desde as sete pontes de Königsberg, (teoria dos) grafos tem sido objeto de intensos estudos que criam algoritmos cada vez mais eficazes para solucionar problemas que antes não seriam resolvidos se não atacados pelo paradigma de vértices e arestas. Este artigo feito para a disciplina do Grafos na Universidade Federal da Fronteira Sul tem como objetivo mostrar e esclarecer algumas aplicações de grafos em aplicações online, mais precisamente na parte de recomendações.

@article{alexanderson2006cover,
  title={About the cover: Euler and K{\"o}nigsberg’s Bridges: A historical view},
  author={Alexanderson, Gerald},
  journal={Bulletin of the american mathematical society},
  volume={43},
  number={4},
  pages={567--573},
  year={2006}
}

\section{Introdução}
Como é dito em \cite{alexanderson2006cover}, Teoria dos Grafos quase certamente teve sua origem quando Leonhard Euler mostrou que era impossível de se resolver um enigma popular e o que era necessário para resolve-lo. O enigma é o que hoje chamamos de "As Pontes de Königsberg. O enigma era tentar descobrir um modo de passar pelas 7 pontes representadas na figura \cite{img:pontes} e terminar no mesmo pedaço de terra que o passeio teve origem sem cruzar a mesma ponte mais de uma vez. Sem saber e taxando o problema como Geometria de Posição, Euler criara um ramo que até hoje vem sendo estudado com afinco por matemáticos e, de uns anos pra cá, cientistas da computação.
\image{}
\caption{Representação das pontes de Königsberg. Os vértices são terra e as arestas são as pontes que as ligavam.}
\label{img:pontes}
Neste artigo falaremos de aplicações de grafos em sistemas de recomendações que são disponibilizadas em serviços online, como recomendações de amigos em redes sociais e recomendações de produtos em e-commerce.
O artigo tem como objetivo apresentar aplicações de grafos e por isso não entrará em detalhes específicos, cabendo ao leitor procurar mais a fundo nas referências ou em outros diversos artigos da área caso tenha interesse mais profundo sobre determinado assunto.
Nas primeiras partes falaremos de aplicações de grafos em recomendações de amigos em redes sociais. Também na área social, falaremos de aplicações em plataformas como Twitter e Google Mail que são usadas para sugerir postagens que se assemelhem ao gosto dos usuários e grupos de conversas por e-mail, respectivamente.
