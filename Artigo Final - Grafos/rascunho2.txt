@inproceedings{yan2012tweet,
  title={Tweet recommendation with graph co-ranking},
  author={Yan, Rui and Lapata, Mirella and Li, Xiaoming},
  booktitle={Proceedings of the 50th Annual Meeting of the Association for Computational Linguistics: Long Papers-Volume 1},
  pages={516--525},
  year={2012},
  organization={Association for Computational Linguistics}
}
• recomenda tweets para usuários
• três grafos: os tweets, os usuários e um outro bipartido ligando os dois
• random walks no grafos
