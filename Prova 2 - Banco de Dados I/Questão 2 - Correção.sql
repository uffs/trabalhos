--A
insert into tipoim (codt, descr)
    values (5, 'Apartamento')
;

--B
delete from imoloca
    where dtloc between '01/01/2012' and '31/12/2012'
        and codl = (select codl
                    from loca
                    where nome = 'Claunir Pavan'
                    )
;

--C
select p.nome, l.descr, l.nome
    from propriet p
        natural join imovel i
        left join imoloca im
        full join loca l
        on im.codl = l.codl
;

--D
select i.descr
    from imovel i
    natural join imoloca im
    where im.vlloc = (select max(vlloc) from imoloca)
;

--E
select p.nome
    from propriet p
        natural join imovel i
    group by p.codp
    having count(*) = (select max(q)
                            from (select count (*) as q
                                from imovel i
                                group by i.codp
                            )
                        )
;

--F
select p.nome
    from propriet p
    join loca l on p.cpf = l.cpf
;
