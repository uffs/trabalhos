-- A
select null as "------------------------------------↓ Questão A";
insert into tipoim (codt, descr)
    values (5, 'Apartamento')
;
-- B
select null as "------------------------------------↓ Questão B";
delete from imoloca i
    where exists (select * from imoloca natural join loca l
                    where (nome = 'Claunir Pavan') and
                    (i.dtloc between '1/1/2012' and '31/12/2012') and
                    i.codl = l.codl
                )
;
-- C
select null as "------------------------------------↓ Questão C";
select distinct p.nome, i.descr, case
                                    when (il.dtsai is null and il.codl is not null) then
                                        l.nome
                                    else 'Sem Inquilino'
                                end as "Inquilino"
    from propriet p
        natural join imovel i
        natural left join imoloca il
        left join loca l on (il.codl = l.codl)
;
-- D
select null as "------------------------------------↓ Questão D";
select i.descr
    from imovel i
    natural join imoloca il
    where il.vlloc = (select max(vlloc) from imoloca)
;
-- E
select null as "------------------------------------↓ Questão E";
select max(nome) from propriet natural join imovel;

-- F
select null as "------------------------------------↓ Questão F";
select distinct p.nome
    from propriet p
    join loca l on p.cpf = l.cpf
;
