<?php
require_once('Pessoa.php');

class Cliente extends Pessoa {
    private $fiado;

    function __construct($n, $c, $t, $f) {
        parent::__construct($n, $c, $t);
        $this->fiado = $f;
    }

    function __toString() {
        return $this->nome . ";" . $this->cpf . ";" . $this->telefone . ";" . $this->fiado;
    }


    function getFiado() { return $this->fiado; }
}
