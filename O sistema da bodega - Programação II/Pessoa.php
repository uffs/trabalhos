<?php
abstract class Pessoa {
    protected $nome, $cpf, $telefone;

    function __construct($n, $c, $t) {
        $this->nome = $n;
        $this->cpf = $c;
        $this->telefone = $t;
    }

    abstract function __toString();

    function setNome($s) { $this->nome = $s; }
    function setCpf($s) { $this->cpf = $s; }
    function setTelefone($s) { $this->telefone = $s; }

    function getNome() { return $this->nome; }
    function getCpf() { return $this->cpf; }
    function getTelefone() { return $this->telefone; }
}
