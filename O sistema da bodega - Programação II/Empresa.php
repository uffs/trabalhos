<?php
require_once('Funcionario.php');

class Empresa {
    private $nome, $cnpj, $produtos, $clientes;
    private $bodegueiros = array();

    function contrata(Funcionario $f) {
        $this->bodegueiros[] = $f;
    }

    function mostra() {
        foreach($this->bodegueiros as $b)
            echo $b . "<br>";
    }
}
