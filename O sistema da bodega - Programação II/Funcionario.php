<?php
include('Pessoa.php');

class Funcionario extends Pessoa {
    private $dataDeAdmissao, $salario;

    function __construct($n, $c, $t, $d, $s) {
        $this->nome = $n;
        $this->cpf = $c;
        $this->telefone = $t;
        $this->dataDeAdmissao = $d;
        $this->salario = $s;
    }

    function __toString() {
        return $this->nome . ";" . $this->cpf . ";" . $this->telefone . ";" . $this->dataDeAdmissao . ";" . $this->salario;
    }
}
