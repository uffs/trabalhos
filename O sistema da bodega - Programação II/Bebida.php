<?php

class Bebida {
    private $nome, $teor, $ml, $preco, $quantidade;

    function __construct($n, $t, $m, $p = NULL, $q = NULL) {
        $this->nome = $n;
        $this->teor = $t;
        $this->ml = $m;
        $this->preco = $p;
        $this->quantidade = $q;
    }

    function __toString() {
        return $this->nome . " " . $this->teor . " " . $this->ml . " " . $this->preco . " " . $this->quantidade;
    }

    function confereEstoque($q) { return $this->quantidade >= $q; }
    function comprar() { $this->quantidade++; }
    function vender($q = 1) { return $this->confereEstoque($q) && $this->quantidade -= $q; }
}
