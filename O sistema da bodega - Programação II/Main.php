<?php
require_once('Empresa.php');
require_once('Funcionario.php');
require_once('Bebida.php');
require_once('Cliente.php');

$e = new Empresa();
$e->contrata(new Funcionario("Antony", "77788899915", "99554477", "15/10/2016", "1500"));
$e->contrata(new Funcionario("Bundinha", "6969696969", "99554477", "15/10/2016", "1500"));
$e->contrata(new Funcionario("Castor", "77788899915", "99554477", "15/10/2016", "1500"));

$b = new Bebida("Tapiroca", 691, 15, 0.0, 10);
echo $b . "<br>";

$cl = new Cliente("DoDouglas", "123321123", "XXXXXX", true);
echo $cl . "<br>";
$e->mostra();
