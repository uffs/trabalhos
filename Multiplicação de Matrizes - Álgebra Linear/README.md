# Como rodar
## Preparação
1. Certifique-se de ter o ruby instalado. Use `ruby -v` e veja se aparece algo. Se não:
    - **Linux**: instale usando `apt-get`.
    - **Windows e OSX**: Se vira.
1. Pelo terminal, vá até a pasta raíz (Esta pasta).
1. Dê permissão para o script `run.sh` com o comando:
```bash
sudo chmod +x run.sh
```

## Calcular uma Entrada
1. Crie um arquivo de entrada `<nome>.in`, trocando `<nome>` pelo nome que você quiser.
1. Dentro do arquivo coloque sua matriz 3x3 de **inteiros**. Exemplo:
```
1 2 3
3 4 5
6 7 8
```
1. Rode o programa com o comando
```bash
./run.sh <vezes> <nome>
```
`vezes` = A potência desejada para a matriz.  
`nome` = O nome do arquivo de entrada (OBS: Apenas o nome, não é necessário colocar a extensão `.in`)
