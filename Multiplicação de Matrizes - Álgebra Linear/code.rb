tamM = 3

matriz = Array.new(tamM) { Array.new(tamM) }

matriz.each_index do |i|
    matriz[i] = STDIN.gets.split(' ')
    matriz[i].each_index do |v| matriz[i][v] = matriz[i][v].to_i end
end

original = Marshal.load(Marshal.dump(matriz))

# Multiplicação
(ARGV[0].to_i - 1).times do |ex|
    aux = Marshal.load(Marshal.dump(matriz))
    tamM.times do |i|
        tamM.times do |j|
            matriz[i][j] = 0
            tamM.times do |k|
                matriz[i][j] += aux[i][k] * original[k][j]
            end
        end
    end
    # puts "\n\nOriginal: " + original.to_s + "\n\n Matriz: " + matriz.to_s + "\n\n Aux: " + aux.to_s + "\n"
end

matriz.each do |a|
    puts a.to_s
end
