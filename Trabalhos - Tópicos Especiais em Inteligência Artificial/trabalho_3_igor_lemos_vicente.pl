% Trabalho III
% Fazer um banco que modele a estrutura de uma faculdade com professores, alunos e disciplinas com seus respectivos horários
% Faça predicados para responder a perguntas sobre este banco de dados:
% 1- Quais disciplinas um professor/aluno leciona/assiste
% 2- Quais são os horários livres de um professor/aluno
% 3- Quais são os horários comuns entre um professor e um aluno
% 4- Quais são os horários livres comuns entre um professor e um aluno
% 5- Quantas disciplinas são ministradas na parte da manhã
% 6- Invente outras 3 perguntas interessantes
% Faça um predicado que teste os predicados criados
% Observação: Os predicados de coleta de soluções devem ser usados, embora em alguns casos outros predicados serão também necessários

% Discente: Igor Lemos Vicente

% ===== 1 =========================================================================
% lista_disciplinas(+Cod, -Res)
% Retorna a lista de disciplinas que o aluno com o código 'Cod' participa, seja como professor ou como aluno.
lista_disciplinas(Cod, Res) :-
  findall(Nome, (aluno(Cod, CodDisciplina), disciplina(CodDisciplina, Nome)), ResAluno),
  findall(Nome, (professor(Cod, CodDisciplina), disciplina(CodDisciplina, Nome)), ResProf),
  flatten([ResProf|ResAluno], Res).

teste1(Lista) :-
  write('\nTeste 1 - Listando todas as disciplinas de Rick Grimes'),
  lista_disciplinas(rick_grimes, Lista),
  write('\nPressione ; para visualizar os outros testes\n\n').

% ===== 2 =========================================================================
% horarios_livres(+Cod, -Res)
% Returna uma lista com os horários livres da pessoa de código 'Cod'
horarios_livres(Cod, Res) :-
  findall((Dia, Periodo), (dia(CodDia, Dia), periodo(CodPeriodo, Periodo)), TodosHorarios),
  horarios_ocupados(Cod, HorariosPessoa),
  remove_lista(HorariosPessoa, TodosHorarios, Res).

teste2(Lista) :-
  write('\nTeste 2 - Listando todos os horários ocupados de Rick Grimes'),
  horarios_ocupados(rick_grimes, Lista),
  write('\nPressione ; para visualizar os outros testes\n\n').

% ===== 3 =========================================================================
% horarios_ocupados_comuns(+Cod, +Cod2, -Resultado)
% Retorna listas dos horarios ocupados em comum entre duas pessoas. A entrada são os códigos dessas pessoas
horarios_ocupados_comuns(Cod, Cod2, Resultado) :-
  setof(Horario, (((aluno(Cod, Dis); professor(Cod, Dis)), horario(Dis, Horario)), ((aluno(Cod2, Dis2); professor(Cod2, Dis2)), horario(Dis2, Horario))), Resultado).

teste3(Lista) :-
  write('\nTeste 3 - Listando os horários ocupados em comuns de Rick Grimes e Hannibal Lecter'),
  horarios_ocupados_comuns(rick_grimes, hannibal_lecter, Lista),
  write('\nPressione ; para visualizar os outros testes\n\n').

% ===== 4 =========================================================================
% horarios_vagos_comuns(+Cod, +Cod2, -Resultado)
% Lista os horários vagos em comum entre as pessoas de código 'Cod' e 'Cod2'
horarios_vagos_comuns(Cod, Cod2, Resultado) :-
  findall((Dia, Periodo), (dia(_, Dia), periodo(_, Periodo)), TodosHorarios),
  horarios_ocupados(Cod, HorariosCod),
  remove_lista(HorariosCod, TodosHorarios, HorariosRefinados1),
  horarios_ocupados(Cod2, HorariosCod2),
  remove_lista(HorariosCod2, HorariosRefinados1, Resultado).

teste4(Lista) :-
  write('\nTeste 4 - Listando todos os horários vagos em comum entre Rick Grimes e Hannibal Lecter\n\n'),
  horarios_vagos_comuns(rick_grimes, hannibal_lecter, Lista),
  write('\nPressione ; para visualizar os outros testes\n\n').

% ===== 5 =========================================================================
% disciplinas_manha(-Resultado)
% Retorna a quantidade de disciplinas que acontecem de manhã.
disciplinas_manha(Resultado) :-
  findall(CodDis, horario(CodDis, (_, manha)), Lista),
  conta_elementos(Lista, Resultado).
disciplinas_manha(0).

teste5(Resultado) :-
  write('\nTeste 5 - Contando quantas disciplinas são realizadas no período da manhã\n\n'),
  disciplinas_manha(Resultado),
  write('\nPressione ; para visualizar os outros testes\n\n').

% ===== 6 =========================================================================
% ======= 6.1 =====================================================================
% Quem é professor E aluno?

% professor_aluno(-Resultado)
% Retorna a lista de todas as pessoas que são alunos E professores
professor_aluno(Resultado) :-
  findall(Nome, (professor(Cod, _), aluno(Cod, _), pessoa(Cod, Nome)), Resultado).

teste61(Resultado) :-
  write('\nTeste 6.1 - Mostrando uma lista de todas as pessoas que são professores e alunos\n\n'),
  professor_aluno(Resultado),
  write('\nPressione ; para visualizar os outros testes\n\n').

% ======= 6.2 =====================================================================
% Devolva uma lista de disciplinas que podem ser cursadas por uma lista de alunos. Isto é, uma lista de disciplinas que ocorre no tempo vago comum entre todos os alunos da lista.

% disciplinas_para_turma(+ListaCodAlunos, -Resposta)
disciplinas_para_turma([Cabeca|Cauda], Resposta) :-
  disciplinas_para_turma(Cauda, RespostaAux),
  horarios_ocupados(Cabeca, HorariosCabeca),
  remove_lista(HorariosCabeca, RespostaAux, Resposta).

teste62(Resultado) :-
  write('\nTeste 6.2 - Listando as disciplinas que podem ser cursadas pelas pessoas Rick Grimes, Dexter e Jackson Teller\n\n'),
  disciplinas_para_turma([rick_grimes, dexter, jackson_teller], Resultado),
  write('\nPressione ; para visualizar os outros testes\n\n').


% ===== Banco de Dados ============================================================
% pessoa(Cod, Nome)
% disciplina(Código, Nome)
% professor(CodPessoa, CodDisciplina)
% aluno(CodPessoa, CodDisciplina)
% periodo(Cod, Nome)
% dia(Código, Nome)
% horario(CodDisciplina, (CodDia, CodPeriodo))

pessoa(dexter, 'Dexter').
pessoa(hannibal_lecter, 'Hannibal Lecter').
pessoa(jackson_teller, 'Jackson Teller').
pessoa(rick_grimes, 'Rick Grimes').
disciplina(sobrevivencia, 'Sobrevivência').
disciplina(lideranca, 'Liderança').
disciplina(assassinato, 'Assassinato').
disciplina(plantacao, 'Plantacao').
professor(hannibal_lecter, assassinato).
professor(rick_grimes, lideranca).
professor(rick_grimes, sobrevivencia).
aluno(jackson_teller, sobrevivencia).
aluno(dexter, assassinato).
aluno(rick_grimes, assassinato).
periodo(manha, 'Manhã').
periodo(tarde, 'Tarde').
periodo(noite, 'Noite').
dia(segunda, 'Segunda-Feira').
dia(terca, 'Terça-Feira').
dia(quarta, 'Quarta-Feira').
dia(quinta, 'Quinta-Feira').
dia(sexta, 'Sexta-Feira').
dia(sabado, 'Sábado').
dia(domingo, 'Domingo').
horario(sobrevivencia, (sexta, noite)).
horario(sobrevivencia, (quarta, noite)).
horario(assassinato, (segunda, noite)).
horario(assassinato, (terca, noite)).
horario(plantacao, (segunda, manha)).
horario(plantacao, (sexta, manha)).
horario(sobrevivencia, (sexta, manha)).

% ===== Testes ====================================================================
teste(Lista) :- teste1(Lista).
teste(Lista) :- teste2(Lista).
teste(Lista) :- teste3(Lista).
teste(Lista) :- teste4(Lista).
teste(Lista) :- teste5(Lista).

% ===== Funções Extras ============================================================
% horarios_ocupados(+Cod, -Res)
% Retorna uma lista dos horários ocupados da pessoa de código Cod
horarios_ocupados(Cod, Res) :-
findall((Dia, Periodo), (aluno(Cod, CodDisciplina), horario(CodDisciplina, (CodDia, CodPeriodo)), dia(CodDia, Dia), periodo(CodPeriodo, Periodo)), ResAluno),
findall((Dia, Periodo), (professor(Cod, CodDisciplina), horario(CodDisciplina, (CodDia, CodPeriodo)), dia(CodDia, Dia), periodo(CodPeriodo, Periodo)), ResProf),
flatten([ResProf, ResAluno], Res).

% remove_lista(+Sublista, +Lista, -Resultado).
% Remove todos os elementos de Sublista da lista Lista
remove_lista([], Lista, Lista).
remove_lista([HSublista|TSublista], Lista, Resultado) :-
  remove_elemento(HSublista, Lista, ListaAux),
  remove_lista(TSublista, ListaAux, Resultado).

% remove_elemento(+Elemento, +Lista, -Resultado)
% Remove o elemento 'Elemento' da 'Lista' e retorna a lista resultando em 'Resultado'
remove_elemento(Elemento, [], []).
remove_elemento(Elemento, [Elemento|Cauda], Aux) :-
  remove_elemento(Elemento, Cauda, Aux).
remove_elemento(Elemento, [Cabeca|Cauda], [Cabeca|Aux]) :-
  remove_elemento(Elemento, Cauda, Aux).

% interseccao(+Lista, +OutraLista, -Resultado)
% Retorna a interseccao entre duas listas
interseccao([], Lista, []).
interseccao([Cabeca|Cauda], Lista, [Cabeca|Resultado]) :-
  pertence(Cabeca, Lista),
  interseccao(Cauda, Lista, Resultado).
interseccao([Cabeca|Cauda], Lista, Resultado) :-
  interseccao(Cauda, Lista, Resultado).

% pertence(+Elemento, +Lista)
% Checa se um elemento pertence a uma lista
pertence(Elemento, []) :- fail.
pertence(Elemento, [Elemento|Cauda]) :- true.
pertence(Elemento, [Cabeca|Cauda]) :- pertence(Elemento, Cauda).

% conta_elementos(+Lista, -Resultado)
conta_elementos([], 0).
conta_elementos([Cabeca|Cauda], Resultado) :-
  conta_elementos(Cauda, ResAux),
  Resultado is ResAux + 1.
