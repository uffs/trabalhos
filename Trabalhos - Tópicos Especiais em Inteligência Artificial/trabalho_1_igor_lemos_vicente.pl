% Feito por Igor Lemos Vicente para a disciplina de Tópicos Especiais em Inteligência Artificial


% 3.7 Conta quantas vezes um dado elemento aparece numa lista
% conta(Alvo, Lista, Resultado)
% Alvo: Elemento que você quer contar na Lista
% Lista:
% Resultado: Variável que terá o resultado de quantas vezes o Alvo aparece em Lista
conta(Alvo, Lista, Resultado) :- conta(Alvo, Lista, 0, Resultado). % Regra
conta(Alvo, [Alvo|Cauda], Contador, Resultado) :- ContadorAuxiliar is Contador + 1, conta(Alvo, Cauda, ContadorAuxiliar, Resultado).
conta(Alvo, [Erro|Cauda], Contador, Resultado) :- conta(Alvo, Cauda, Contador, Resultado).
conta(Alvo, [], Contador, Contador). % Fato

% 3.9 Retorna o item na posição especificada de uma lista
% posicao(Posicao, Lista, Resultado)
% Posicao:
% Lista:
% Resultado: Variável que conterá o elemento da Lista na posição Posicao
posicao(Posicao, Lista, Resultado) :- posicao(Posicao, Lista, 0, Resultado). % Chamada inicial da função.
posicao(Posicao, [Item|Cauda], Posicao, Item). % A posição que procuram é a posição que a função se encontra.
posicao(Posicao, [Teste|Cauda], Contador, Resultado) :- ContadorAux is Contador + 1, posicao(Posicao, Cauda, ContadorAux, Resultado). % Não achou, avança.
posicao(Posicao, [], Contador, null). % Chegou ao final da lista. Se apagar essa linha, vai dar fail e semânticamente é igual.

% 3.10 Recebe duas listas e testa se a primeira lista pertence à segunda lista
% contido_na_lista(Sublista, Lista, Resultado)
% Sublista:
% Lista:
% Resultado: É true ou false dependendo se Sublista esteja contida em Lista
procura_na_lista(Alvo, [], false) :- false.
procura_na_lista(Alvo, [Alvo|Cauda], true).
procura_na_lista(Alvo, [Cabeca|Cauda], Resultado) :- procura_na_lista(Alvo, Cauda, Resultado).

contido_na_lista([], Lista2, true).
contido_na_lista([Cabeca|Cauda], Lista2, Resultado) :- procura_na_lista(Cabeca, Lista2, ResultadoAux1), contido_na_lista(Cauda, Lista2, Resultado).

% 3.11 Recebe duas listas e testa se uma lista pertence à outra lista e retorna a lista e sublista, nesta ordem
% contido_na_lista(Sublista, Lista, VarSublista, VarLista)
% Sublista:
% Lista:
% VarSublista: Variável que receberá a Sublista
% VarLista: Variável que receberá a Lista
contido_na_lista(Sublista, Lista, Sublista, Lista).
contido_na_lista(Sublista, Lista, RetornoLista, RetornoSublista) :- contido_na_lista(Sublista, Lista), contido_na_lista(Sublista, Lista, Sublista, Lista).

% 3.13 Recebe duas lista e as concatena
% concatena_lista(Lista, Lista2, Resultado)
% Lista:
% Lista2:
% Resultado: Variável que conterá a concatenação de Lista e Lista2
concatena_lista([], Resultado, Resultado).
concatena_lista([Cabeca|Cauda], Lista, [Cabeca|Resto]) :- concatena_lista(Cauda, Lista, Resto).

% 3.14 Recebe duas listas e subtrai a segunda da primeira
% remove_sublista(Sublista, Lista, Resultado)
% Sublista:
% Lista:
% Resultado: Variável que conterá o resultado de Lista - Sublista
remove_elemento(Alvo, [], []).
remove_elemento(Alvo, [Alvo|Cauda], Resultado) :- remove_elemento(Alvo, Cauda, Resultado).
remove_elemento(Alvo, [Cabeca|Cauda], [Cabeca|Resultado]) :- remove_elemento(Alvo, Cauda, Resultado).

remove_sublista([Cabeca|Cauda], Lista, Resultado) :- remove_elemento(Cabeca, Lista, ResultadoAux), remove_sublista(Cauda, ResultadoAux, Resultado).
remove_sublista([], Lista, Lista).





% ==================================================================================================

teste :- write('\n3-7 Conta quantas vezes um dado elemento aparece numa lista\n'),
write('TESTE: Procurando 10 em [1,0,4,9,10,7,0]\n'),
conta(10, [1,0,4,9,10,7,0], Resultado1),
write(Resultado1),

write('\n\n3-9 Retorna o item na posicao especificada de uma lista\n'),
write('TESTE: Retornando a posicao 3 de [0,x,8,eita,829]\n'),
posicao(3, [0,x,8,eita,829], Resultado2),
write(Resultado2),

write('\n\n3-10 Recebe duas listas e testa se a primeira lista pertence a segunda lista\n'),
write('TESTE: Checando se [1,2,99] está contido em [1, 5, 8, 99, x, 2]\n'),
contido_na_lista([1,2,99], [1,5,8,99,342,2], Resultado3),
write(Resultado3),

write('\n\n3-11 Recebe duas listas e testa se uma lista pertence à outra lista e retorna a lista e sublista, nesta ordem\n'),
write('TESTE: Checando se [1,2,99] está contido em [1, 5, 8, 99, x] e retornando ambas as listas\n'), contido_na_lista([1,2,99], [1,5,8,99,34], Sublista, Lista),
write(Sublista), write(Lista),

write('\n\n3-13 Recebe duas lista e as concatena\n'),
write('TESTE: Concatenando [1,2,3] e [4,5,6]\n'),
concatena_lista([1,2,3], [4,5,6], Resultado4),
write(Resultado4),

write('\n\n3-14 Recebe duas listas e subtrai a segunda da primeira\n'),
write('TESTE: Subtraindo [1,2,3] de [1,2,3,4,5,6,7]\n'),
remove_sublista([1,2,3], [1,2,3,4,5,6,7], Resultado5),
write(Resultado5).
