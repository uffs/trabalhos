% Jogo de Xadrez

% tabuleiro([_,_,_,_,_,_,_,_,_,]).


% --------------------------------OBSERVAÇÕES--------------------------------------
% Não estou passando o tabuleiro nem a sequência. Cada método que for usa-los, buscar-los-á no banco de dados.
% Só falta fazer a lógica do computador escolher o movimento.
% A ideia é ele procurar uma sequencia igual a atual e ir calculando, para cada próximo movimento, quanto vale a pena jogá-lo.
% ---------------------------------------------------------------------------------



% Mensagem inicial
comeca_partida :-
  novo_tabuleiro,
  imprime_tabuleiro,
  write('Faça sua jogada utilizando o predicado joga(Posicao). Seu símbolo é # e o do computador é @'),
  recorda(sequencia_atual, ([], _)).

joga(Posicao) :-
  jogo_nao_vencido('@'),
  marca_posicao(Posicao, '#'),
  imprime_tabuleiro,
  jogada_computador.
  joga(_) :- write('Posicao inválida, tente novamente.').

% Computador faz a jogada.
jogada_computador :-
  jogo_nao_vencido('#'),
  write("Hum... Deixe-me pensar..."), nl,
  % melhor_jogada(Posicao),
  marca_posicao(5, '@'),
  recorded(sequencia_atual, (Sequencia, _)),
  % imprime_tabuleiro,
  % write('Sua vez.').
  write(Sequencia), nl,
  imprime_tabuleiro.

% marca_posicao(+Posicao, +Simbolo, +Tabuleiro, -NovoTabuleiro)
% Marca como Simbolo na Posicao do Tabuleiro. Retornar false se posição não existe ou não está disponível.
marca_posicao(Posicao, Simbolo) :-
  recorded(tabuleiro, Tabuleiro),
  marca_posicao(Posicao, Simbolo, Tabuleiro, NovoTabuleiro),
  recorda(tabuleiro, NovoTabuleiro).
marca_posicao(_, _, [], _) :- !. % Chegou ao fim do tabuleiro e não encontrou a posição.
marca_posicao(Posicao, Simbolo, [Posicao|Resto], [Simbolo|Resto]) :- % Se achou a posição, marca no tabuleiro e grava a sequência.
  recorded(sequencia_atual, (Sequencia, _)),
  flatten([Sequencia|Posicao], SequenciaAux),
  recorda(sequencia_atual, (SequenciaAux, _)).
marca_posicao(Posicao, Simbolo, [Cabeca|Cauda], [Cabeca|Resto]) :-
  marca_posicao(Posicao, Simbolo, Cauda, Resto).

% melhor_jogada(+Sequencia, -Posicao)
% Devolve a melhor posição a ser jogada pelo computador.

% jogo_nao_vencido
% retorna true se ninguém venceu ainda
% se alguém venceu, imprime mensagem, salva sequência e dá fail.
jogo_nao_vencido(Simbolo) :-
  venceu_coluna(Simbolo);
  venceu_linha(Simbolo);
  venceu_diagonal(Simbolo),
  write('Jogo terminado. '),
  write(Simbolo),
  write(' vence o jogo.'), nl, nl,
  recorded(sequencia_atual, (Sequencia, _)),
  recorda(sequencia_atual, (Sequencia, Simbolo)),
  !.
jogo_nao_vencido(_, _).

% Predicados Auxiliares------------------------------------------------------------
% Predicados auxiliares que retornam true caso haja situação vitória.
venceu_coluna(Simbolo) :-
  recorded(tabuleiro, [Simbolo, _, _, Simbolo, _, _, Simbolo, _, _]);
  recorded(tabuleiro, [_, Simbolo, _, _, Simbolo, _, _, Simbolo, _]);
  recorded(tabuleiro, [_, _, Simbolo, _, _, Simbolo, _, _, Simbolo]).
venceu_linha(Simbolo) :-
  recorded(tabuleiro, [Simbolo, Simbolo, Simbolo, _, _, _, _, _, _]);
  recorded(tabuleiro, [_, _, _, Simbolo, Simbolo, Simbolo, _, _, _]);
  recorded(tabuleiro, [_, _, _, _, _, _, Simbolo, Simbolo, Simbolo]).
venceu_diagonal(Simbolo) :-
  recorded(tabuleiro, [Simbolo, _, _, _, Simbolo, _, _, _, Simbolo]);
  recorded(tabuleiro, [_, _, Simbolo, _, _, Simbolo, _, Simbolo, _, _]).

% imprime_tabuleiro(+Tabuleiro)
% Imprime o tabuleiro no terminal
imprime_tabuleiro :-
  recorded(tabuleiro, [A, AA, AAA, B, BB, BBB, C, CC, CCC]),
  write(A),
  write('|'),
  write(AA),
  write('|'),
  write(AAA),
  nl,
  write('-----'),
  nl,
  write(B),
  write('|'),
  write(BB),
  write('|'),
  write(BBB),
  nl,
  write('-----'),
  nl,
  write(C),
  write('|'),
  write(CC),
  write('|'),
  write(CCC),
  nl, nl.

% novo_tabuleiro(-NovoTabuleiro).
novo_tabuleiro :-
  recorda(tabuleiro, [1,2,3,4,5,6,7,8,9]).
