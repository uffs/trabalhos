% Feito por Igor Lemos Vicente (Auxiliado por Marco Aurélio Alves Puton) para a disciplina de Tópicos Especiais em Inteligência Artificial

% Trabalho II
% Dada a base de dados abaixo, implemento os seguintes predicados:
% - Predicado que lista os clientes de uma filial específica =======================================
itera([]).
itera([H|T]) :- lista(H), itera(T).

lista(CODIGO) :- cliente(CODIGO, NOME, IDADE, PROFISSAO, BEM), write(NOME), nl.
listaclientes(FILIAL) :- filial(FILIAL, CODES), itera(CODES).

% - Predicado que lista os clientes de toda a empresa ==============================================
lista :- filial(X, Y), write(X), nl, listaclientes(X), nl, fail.

% - Predicado que lista as profissões dos clientes de toda a empresa ===============================
profissoes :- cliente(CODIGO, NOME, IDADE, PROFISSAO, BEM), write(NOME), write(": "), write(PROFISSAO), nl, fail.

% - Predicado que lista todos os bens adquiridos por um cliente ====================================
itera2([]).
itera2([H|T]) :- write(H), nl, iterabem(H), write('AAAAA'), itera2(T), write('OLOCO').

iterabem([]).
iterabem(GOAL) :- call(GOAL), nl, write(VAL), write(" "), iterabem(REST).

benscliente(NOME) :- write(NOME), nl, cliente(CODIGO, NOME, IDADE, PROFISSAO, BENS), itera2(BENS).

% - Predicado que conta quantos clientes a empresa possui ==========================================
nclientes(X) :- findall(N, cliente(CODIGO, NOME, IDADE, PROFISSAO, BEM), Ns), length(Ns, X).

% - Predicado que calcula o custo médio dos bens ===================================================

% Base de dados: ===================================================================================
filial('POA', [2345, 2346, 2347]).
filial('Curitiba', [3567, 3568, 3569, 3570]).

cliente(2345, 'Carlos', 45, 'Médico', [bem(c, 'Mercedes', 80000)]).
cliente(2346, 'Rogério', 23, 'Estudante', [bem(m, 'Kawasaki 1000', 24000)]).
cliente(2347, 'Marcelo', 52, 'Advogado', [bem(c, 'Audi A4', 57000), bem(c, 'Ford Focus', 25000)]).
cliente(3567, 'Ana', 35, 'Modelo', [bem(c, 'BMW Serie 3', 42000)]).
cliente(3568, 'Marcos', 20, 'Operário', [bem(c, 'Fiat Uno', 20000)]).
cliente(3569, 'José', 45, 'Professor', [bem(c, 'Fiat Pálio 1.6', 30000)]).
cliente(3570, 'João Carlos', 38, 'Médico', [bem(c, 'Ford Fox', 35000), bem(m, 'CB 450', 15000)]).
% ==================================================================================================
