\documentclass[journal]{IEEEtran}
\usepackage{blindtext}
\usepackage{graphicx}
\usepackage{lipsum}
\usepackage{amsmath}
\usepackage{float}

% *** CITATION PACKAGES ***
\usepackage{cite}

%Acentuação
\usepackage[utf8]{inputenc}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}

\title{Bitcoin e o Problema dos Generais Bizantinos}

\author{Igor Lemos Vicente}


\maketitle
\pagenumbering{gobble}

\begin{abstract}
Bitcoin é uma tecnologia de moeda virtual criada com o intuito de eliminar intermediários em transações monetárias e que encontra uma solução heurística - específica para uma aplicação - para o problema dos Generais Bizantinos usando criptografia e assinaturas digitais.\\
Este artigo tem como objetivo explicar de uma forma clara e com um nível maior de abstração a importância da solução deste problema e a solução empregada pela tecnologia do Bitcoin através da Blockchain.
\end{abstract}


\begin{IEEEkeywords}
Bitcoin, Problema dos Generais Bizantinos, Blockchain.
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle

\section{Introdução}
Bitcoin é uma moeda virtual criada no ano de 2008 por Satoshi Nakamoto - um pseudônimo -, com o propósito maior de eliminar a necessidade de uma entidade central entre transações feitas \textit{online} e com isso reduzir o custo das operações para viabilizar transferências de quantias menores do que é possível nos dias de hoje. A tecnologia funciona em uma rede \textit{peer-to-peer} chamada de Blockchain por conter blocos que possuem informações sobre transações feitas. Essas informações são públicas e é isso que garante a não necessidade de uma entidade central. Apesar de públicas, as transações contém apenas endereços das carteiras dos usuários de Bitcoin, tornando o anonimato garantido se tomadas certas precauções.\\
O problema de se eliminar terceiros de uma transação é garantir que o que as partes dizem ter realmente sejam delas, ou seja, ambos tem que acreditar que o outro está dizendo a verdade, o que a seguir veremos que é a ideia por trás do problema dos Generais Bizantinos. A solução heurística feita pela tecnologia do Bitcoin para esse problema de confiança torna a própria confiança desnecessária. Um não precisa mais confiar em outro, pois a Blockchain tirar as dúvidas entre as duas partes da operação. No artigo referiremo-nos a Bitcoin tanto como "o Bitcoin" como "a tecnologia Bitcoin".

\section{O problema dos Generais Bizantinos}
O problema dos Generais Bizantinos, descrito por Lampert \cite{tbgp}, pode ser expresso da seguinte forma:\\
Um general e seus tenentes cercam uma cidade. Eles precisam que pelo menos metade mais um dos generais decidam entre atacar ou não atacar a cidade. Caso eles ataquem com menos que o necessário, eles perdem a batalha e todos os atacantes morrem.\\
A única forma deles se comunicarem é atráves de mensageiros e mensagens orais, ou seja, um recebedor só saberá com certeza quem lhe enviou a mensagem e nada mais que isso. Não há preocupação em modificações feitas por terceiros nas mensagens enviadas.\\
Parece uma tarefa simples, mas suspeita-se que haja traidores entre estes comandantes que estão cercando a cidade. Nesse grupo de traidores pode estar inclusive o general. Com estas informações, o problema pede uma solução que faça com que os metade mais um dos comandantes decidam na mesma ordem e a executem.\\
De forma mais simples, temos dois principais objetivos:\\
1. Todos os comandantes leais devem obedecer a mesma ordem.\\
2. Se o general é leal, então todos os tenentes devem obedecer a ordem que ele manda.\\
Lampert mostra que há uma solução para esse problema quando o número de comandantes é $3m + 1$ e o número de traidores é no máximo $m$.

\section{O algoritmo das mensagens orais}
Nessa seção iremos apresentar o algoritmo das mensagens orais de uma forma mais sucinta do que a apresentada em \cite{tbgp}, embora aquela seja totalmente baseada nesta.
Primeiro definimos que mensagens orais tem as seguintes propriedades:\\
A1. Toda mensagem que é enviada é entregue corretamente.\\
A2. O recebedor de uma mensagem sabe quem a enviou.\\
A3. A ausência de uma mensagem pode ser detectada.\\
Além disso, assumimos que cada general pode enviar mensagem para todos os outros comandantes.\\
Temos também que a ordem padrão dos comandantes é recuar, caso o general seja o traidor e não envie mensagem alguma.\\
Agora, para fins de demonstração, trocaremos as ordens por quaisquer valores. Pense como se eles estivessem combinando a hora do ataque ao invés de decidir se vão atacar ou não.\\
$n :=$ número de comandantes.\\
Primeiro define-se uma função \textit{maioria} que pode ter 2 escolhas de retorno: O valor $v$que seja maioria em um conjunto de valores caso este exista ou o valor padrão caso contrário; a mediana entre os valores $v$ de um conjunto ordenado.\\
Agora temos o algoritmo que faz uso das propriedades e função descrita:
$m=$ número de traidores\\
$3m+1$ = número de generais\\
\\
Algoritmo OM(0):\\
(1) O comandante envia seu valor para todos os tenentes.\\
(2) Cada tenente usa o valor recebido pelo comandante ou \textit{recuar} (valor padrão) caso não tenha recebido nenhum valor.\\
\\
Algoritmo OM($m$), $m > 0$:\\
(1) O comandante envia seu valor para todos os tenentes.\\
(2) Para cada $i$, $v_{i}$ é o valor que o tenente $i$ recebe do comandante ou \textit{recuar} caso ele não receba nada. O tenente $i$ agirá como comandante no Algoritmo OM($m-1$) para enviar o valor $v_{i}$ para os $n-2$ outros tenentes.\\
(3) Para cada $i$ e cada $j \neq i$, $v_{j}$ é o valor que o tenente $i$ recebe do tenente $j$ no passo (2) (Usando o Algoritmo OM($m-1$)) ou \textit{recuar} se ele não recebeu valor algum. Tenente $i$ obedece a ordem \textit{maioria}($v_{1}, \dots, v_{n-1}$).



\section{A Impossibilidade da Solução Não-Heurística}
Embora haja soluções para casos específicos ou com limitações para o problema, não existe algoritmo que resolva-o completamente e é provado que é impossível de se obter tal algoritmo através de sua forma reduzida, chamada de Problema dos Dois Generais, que pode ser expressa da forma:\\
Imagine que exista um vale onde reside três exércitos na ordem amigo, inimigo e amigo. Os dois exércitos amigos juntos possuem mais homens que o exército inimigo, mas menos quando estão separados, por isso precisam atacar o exército inimigo na mesma hora ou perderão. Os generais de cada exército amigo precisam combinar a hora do ataque enviando um mensageiro um ao outro (O mensageiro terá que passar pelas linhas inimigas para chegar ao outro lado do vale.\\
Digamos que o general do exército A envie a hora do ataque para o general do exército B. Não há forma de o general A ter certeza que a mensagem foi enviada. O único jeito de saber que foi é se o general do exército B enviar uma confirmação de volta, mas isso implica que agora o general do exército B não terá certeza se a mensagem de confirmação foi enviada, e isso causará um ciclo infinito.\\
Se pudermos garantir que um general possa ser o última a enviar uma mensagem e essa seja a 50ª mensagem, teremos também que ele pode enviar 1 mensagem a menos. Se ele pode enviar uma mensagem a menos, ele pode enviar 2 a menos, e assim cada vez menos até nenhuma mensagem ser enviada, causando uma contradição, pois se não há mensagem, como saberão a hora que ambos devem atacar?

\section{Aplicabilidade da Solução}
A solução para o problema dos Generais Bizantinos teria uma enorme utilidade nos dias de hoje. Uma das áreas que se beneficiariam com isso seria a do processamento distribuído, onde há cada vez mais processadores trabalhando paralelamente para seguir a lei de Moore, que diz que estes devem dobrar a capacidade a cada 18 meses \cite{mlppf}.\\
A área de computação distribuida, que tem como objetivo manter a computação descentralizada também teria um grande uso para a solução do problema. A aplicabilidade que discutiremos aqui é para uma nova forma descentralizada de moeda.

\section{Assinatura Digital}
Definiremos aqui o que é uma assinatura digital para entendimento de colocações futuras.\\
Assinatura digital é uma forma de garantir que a mensagem vista por uma pessoa B que teve como origem uma pessoa A conhecida não tenha sido modificada no caminho. Essa assinatura se dá por chaves e senhas que, de forma matemática, transformam informação em uma cadeia de caracteres única que são usadas para comprovar a identidade de alguém. Um exemplo de assinatura digital é a SHA-1\cite{shs}, embora esta não seja mais considerada segura \cite{fcitfs1}.

\section{Double Spending}
Tirar intermediários das transações tem efeitos colaterais, e um deles é o \textit{double spending}, termo que se refere à ação em que um indivíduo gasta duas vezes o mesmo dinheiro. No caso da tecnologia do Bitcoin, porém, a Blockchain toma a forma da entidade controladora que evita o \textit{double spending}.

\section{Blockchain}
A proposta da Blockchain, em termos abstratos, é, através de transações públicas, garantir que todos saibam e entrem em um consenso sobre quanto cada indivíduo possui e pode gastar. Essa solução para o \textit{double spending} é feita de forma elegante e pode ser descrita da seguinte maneira, baseada na publicação de Nakamoto \cite{bapecs}:\\
Digamos que você faça uma transação para um amigo seu, garantindo a autenticidade de sua identidade por uma assinatura digital. Enquanto isso, cada nó da rede trabalha em uma charada matemática que definirá um novo bloco na corrente. Um nó, ao conseguir a solução para essa charada, finaliza o bloco com as últimas transações lançadas na rede (incluindo a sua), e coloca-o na corrente. A rede foi configurada para que essa solução leve em média 10 minutos para ser alcançada. Após isso, o nó envia essa nova corrente a todos os outros da rede, que começam a trabalhar nessa nova versão e consequentemente numa charada nova. Agora a transação faz parte da corrente, mas como ela está listada no bloco mais recente, não é fortemente confiável, isso porque as transações tornam-se concreta a partir de um número não tão alto de bloco (De 12 pra cima). Isso porque, se a transação já está há 12 blocos (em média 120 minutos) na corrente, significa, probabilísticamente, que a maioria dos nós está trabalhando nessa corrente, tornando-a a mais aceita. Para você re-utilizar essa quantia de Bitcoin e executar o double spendig, você teria que refazer o trabalho de todos estes 12 blocos mais o que está sendo trabalhado pela rede agora, tornando sua corrente maliciosa a certa. Isso mostra-se improvável, visto que teu nó teria que trabalhar em menos de 10 minutos o que a maioria da rede trabalhou em 120. A seguir explicaremos temos uma explicação formal sobre essa probabibilidade.

\section{Probabilidade de Double Spending na Bitcoin}
O ataque de double spending se daria num caso onde uma transação seja cancelada por uma corrente maliciosa após uma transferência ser realizada, deixando o recebedor sem o que ele achava ter recebido.\\
Sabemos que a probabilidade de conseguir resolver um novo bloco se dá pela capacidade de processamento de um nó.\\
Para efeito de mostragem e seguindo \cite{bapecs}, temos:\\
\\
$h =$ probabilidade da rede honesta resolver um bloco\\
$a =$ probabilidade da rede maliciosa (Atacante) resolver um bloco\\
$a_{b} =$ probabilidade de uma rede maliciosa alcançar uma rede honesta após b blocos\\
\[
a_{b} =
\begin{cases}
    1, & \text{se } h \leq a\\
    (\frac{a}{h})^{b}, & \text{se } h > a
\end{cases}
\]


Assumindo que a rede honesta comanda mais da metade do processamento, a probabilidade de um atacante conseguir realizar o double spending cai exponencialmente.\\
Ainda em \cite{bapecs}, agora vamos encontrar o número de blocos que o recebedor precisa esperar ser resolvido para ficar suficientemente certo de que não terá sido vítima de um ataque.\\
O recebedor gera uma chave e dá ao remetente logo antes do acordo. Isso previne que o remetente prepare uma corrente a frente do tempo resolvendo os blocos até ter a sorte de passa a corrente honesta.\\
Após a transação ser enviada para a Blockchain, o remetente começa a trabalhar numa nova corrente maliciosa.\\
O recebedor aguarda até a transação ser fixada em um bloco e $b$ serem ligados após este. Ele não sabe o exato progresso que o atacante fez, mas assumindo que a corrento honesta levou o tempo médio esperado por bloco, o potencial progresso do atacante vai ser uma distribuição de Poisson com o valor esperado de:\\
\[
\lambda = z \frac{a}{h}
\]\\
Para conseguir a probabilidade do atacante alcançar a corrente honesta, multiplicamos a densidade de Poisson por cada quantidade de progresso que ele pode ter feito pela probabilidade de ele alcançar naquele momento:\\
\[
1 - \sum_{k=0}^{\infty}
\frac{\lambda^{k}e^{-\lambda}}{k!}.
\begin{cases}
(\frac{a}{h}) ^{(b-k)}, & \text{se } k \leq b\\
1, & \text{se } k > b
\end{cases}
\]\\
Arrumando para evitar a soma infinita da cauda da distribuição:\\
\[
1-\sum^{b}_{k=0} \frac{\lambda^{k}e^{-\lambda}}{k!}(1-(\frac{a}{h})^{(b-k)})
\]

O autor converte a fórmula para um código na linguagem de Programação C e calcula a probabilidade do atacante realizar o \textit{double spending}, obtendo os números da tabela \ref{tab:tabp}.

\begin{table}[H]
    \[
    \begin{tabular}{ll}
        b=0 & A=1.0000000\\
        b=5 & A=0.1773523\\
        b=10 & A=0.0416605\\
        b=15 & A=0.0101008\\
        b=20 & A=0.0024804\\
        b=25 & A=0.0006132 \\
        b=30 & A=0.0001522 \\
        b=35 & A=0.0000379 \\
        b=40 & A=0.0000095 \\
        b=45 & A=0.0000024 \\
        b=50 & A=0.0000006\\
    \end{tabular}
    \]
\caption{Probabilidade de o atacante ter sucesso no ataque com $a = 0.3$ (O atacante ter $30\%$ do processamento da rede)}
\label{tab:tabp}
\end{table}

\section{Conclusão}
A partir da revisão bibliográfica foi possível constatar que embora haja muitos problemas comprovadamente sem solução, há alguns que poderão ser analisados de um ponto de vista diferente, levando a soluções não exatas, mas bem próximas da exatidão para aplicações específicas.
Generais Bizantinos é um problema que hoje em dia pode ser visto na internet e em como as pessoas e aplicações se comunicam. Resolve-lo, como vimos, é impossível, mas as soluções heurísticas e não-exatas que existem hoje são o que garantem, dentre tantas outras coisas, nossa navegação na \textit{web}.\\
Bitcoin, mais do que uma inovação computacional, é também uma inovação econômica e social, trazendo de volta o domínio sobre o que é nosso sem a interferência de terceiros.


\ifCLASSOPTIONcaptionsoff
  \newpage
\fi

\newpage
\bibliography{referencias_teoria.bib}{}
\bibliographystyle{unsrt}


\end{document}
