Trabalho de implementação do método de Gram-Schmidt para ortonormalização de vetores no plano R⁵.  
Feito por Igor Lemos Vicente, graduando em Ciência da Computação pela Universidade Federal da Fronteira Sul, para a disciplina de Álgebra Linear ministrada pelo professor Antônia Marcos Correa Neri.
