A entrada precisa ser formatada corretamente. Veja entrada_exemplo.txt para saber como criá-las.

Para usar o programa:
1. Abra o terminal e navegue até a pasta do arquivo main.cpp.
2. Digite o comando $ g++ main.cpp -O2
3. Após isso, um arquivo a.out será criado na pasta.
4. Faça uma entrada no padrão do entrada_exemplo.txt
5. No terminal, digite $ ./a.out < (entrada)
        Substitua (entrada) com o nome do seu arquivo
6. Os vetores normalizados serão exibidos no terminal.

Se quiser que a saída vá para um arquivo, ao invés de fazer o passo 5, digite $ ./a.out < (entrada) > (saida)
Substituia (entrada) pelo nome do arquivo de entrada e (saida) pelo nome do arquivo que você quer que receba a resposta.
