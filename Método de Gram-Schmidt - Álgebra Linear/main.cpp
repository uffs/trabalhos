#include <cstdio>
#include "Vetor.cpp"
#include <cmath>

#define MAX 11234
#define ERRO 10e-7

bool eBase(Vetor v[]) {
    double multiplicador;
    Vetor vetores[5];
    //cria uma cópia dos vetores para não modificar os originais
    for (int i = 0; i < 5; i++) vetores[i] = v[i];

    //escalona a "matriz"
    for (int i = 0; i < 5; i++) {
        for (int j = i+1; j < 5; j++) {
            multiplicador = vetores[j].getElemento(i) ? -vetores[i].getElemento(i) / vetores[j].getElemento(i) : 1;
            for (int k = 0; k < 5; k++) {
                vetores[j].setElemento(k, vetores[j].getElemento(k) * multiplicador + vetores[i].getElemento(k));
            }
        }
    }

    //checa se o produto da diagonal principal da 0
    multiplicador = 1;
    for (int i = 0; i < 5; i++) {
        multiplicador *= vetores[i].getElemento(i);
    }
    return -ERRO < multiplicador && multiplicador < ERRO;
}

void gramSchmidt(Vetor entrada[], Vetor saida[]) {
    for (int i = 0; i < 5; i++) { saida[0].setElemento(i, entrada[0].getElemento(i)); }

    for (int n = 1; n < 5; n++) {
        Vetor soma(0);
        for (int i = 0; i < n; i++) {
            soma = soma + ( ( entrada[n].produto(saida[i]) / saida[i].produto(saida[i]) ) * saida[i] );
        }
        saida[n] = entrada[n] - soma;
    }

    for (int i = 0; i < 5; i++) {
        double produto = sqrt(saida[i].produto(saida[i]));
        for (int j = 0; j < 5; j++) {
            saida[i].setElemento(j, saida[i].getElemento(j) / produto);
        }
    }
}

int main (void) {
    Vetor entrada[5], saida[5];
    double aux;
    char lixosNaEntrada[MAX];

    printf("Vetores passados:\n");
    //le os números
    fgets(lixosNaEntrada, MAX, stdin);
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
        getchar();
        scanf("%lf", &aux);
        entrada[i].setElemento(j, aux);
        }
        entrada[i].print();
        fgets(lixosNaEntrada, MAX, stdin);
    }
    printf("_____________\n");

    if (eBase(entrada)) {
        gramSchmidt(entrada, saida);
        printf("Vetores resultado:\n");
        for (int i = 0; i < 5; i++) { saida[i].print(); }
    }
    else {
        printf("Os vetores dados não formam uma base pois não são linearmente dependentes.\n");
    }
    return 0;
}
