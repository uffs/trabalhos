class Vetor {
private:
    double elementos[5];

public:

    //construtores
    Vetor(double a=0, double b=0, double c=0, double d=0, double e=0) {
        this->elementos[0] = a;
        this->elementos[1] = b;
        this->elementos[2] = c;
        this->elementos[3] = d;
        this->elementos[4] = e;
    }

    //getters e setters
    void setElemento(int i, double valor) { this->elementos[i] = valor; }
    double getElemento(int i) { return this->elementos[i]; }

    //métodos
    double produto(Vetor b) {
        double s = 0.0;
        for (int i = 0; i < 5; i++) {
            s += this->getElemento(i) * b.getElemento(i);
        }
        return s;
    }

    void print() {
        printf("(");
        for (int i = 0; i < 5; i++) {
            printf("%7.4lf", this->getElemento(i));
            if (i == 4) printf(")");
            else printf(", ");
        }
        printf("\n");
    }

    //operadores
    friend Vetor operator+(Vetor a, Vetor b);
    friend Vetor operator-(Vetor a, Vetor b);
    friend Vetor operator*(double b, Vetor a);
};

Vetor operator+(Vetor a, Vetor b) {
    Vetor saida;
    for (int i = 0; i < 5; i++) saida.setElemento(i, a.getElemento(i) + b.getElemento(i));
    return saida;
}

Vetor operator-(Vetor a, Vetor b) {
    Vetor saida;
    for (int i = 0; i < 5; i++) saida.setElemento(i, a.getElemento(i) - b.getElemento(i));
    return saida;
}

Vetor operator*(double b, Vetor a) {
    Vetor saida;
    for (int i = 0; i < 5; i++) saida.setElemento(i, a.getElemento(i) * b);
    return saida;
}
