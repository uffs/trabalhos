1 apt-get install llvm clang
2 clang -emit-llvm hello.c -c -o hello.bc
3 lli hello.bc
4 llc -march=arm hello.bc -o hello.s
5 arm-elf-gcc hello.s -o hello.elf
6 clang -march=armv7-a -ccc-host-triple arm-elf -ccc-gcc-name arm-elf-gcc hello.c
