@1 Lê do Console -------------------------------------
  .global _start

  @Porta de entrada do teclado. Nessa porta estará o último dígito pressionado, de acordo com a documentação do simulador
  .set LOCAL_DADOS_TECLADO,  0x90010
  @Porta de status do teclado. Essa porta dirá se o teclado foi pressionado e o dígito ainda não foi lido
  .set LOCAL_STATUS_TECLADO, 0x90011 @Porta de estado do teclado
  .set BIT_MENOS_SIGNIFICATIVO,      0x1

_start:
  @configuração do console (Simulador)
  mov r0, #0
  ldr r1, =mensagem_inserida
  ldr r2, =tamanho_mensagem
  mov r7, #3
  svc 0x55 @syscall. A mensagem está gravada na memória
  ldr r3, =tamanho_mensagem
  str r0, [r3]
@2 Criptografa ---------------------------------------
@r0 tem o tamanho da mensagem
@r4 vai ser o auxiliar pra trabalhar com os bytes
@r5 é o iterador
@r6 vai ter o vetor de inicialização ou o bloco anterior
@r8 chave
@r9 é o auxiliar que recebe a subtração entre r5 e r0 pra fazer o teste de condição
@r12 endereço da mensagem inserida
ldr r8, =chave
ldr r8, [r8]
ldr r12, =mensagem_inserida
mov r5, #0
ldr r6, =vetor_inicializacao
ldr r6, [r6]
blocacao:
  ldr r4, [r12, r5]
  eor r6, r4, r6 @XOR entre mensagem e bloco anterior (ou vetor de inicialização)
  eor r6, r6, r8 @XOR o resultado e a chave
  str r6, [r12, r5] @Sobrescreve o bloco da mensagem com o bloco criptografado
  add r5, #4
  subs r9, r5, #1024 @O valor certo é 1024 (o tamanho máximo da palavra)
  bmi blocacao

@3 Lê do Teclado Numérico ----------------------------

tentativa_descriptografar:
  ldr	r1,=LOCAL_STATUS_TECLADO
le_asterisco:
  ldr	r2,[r1]
  tst     r2,#BIT_MENOS_SIGNIFICATIVO
  beq     le_asterisco
  ldr	r4,=LOCAL_DADOS_TECLADO
  ldr	r3,[r4] @Passa o valor digitado no teclado

@4 Checa se é *: -------------------------------------
  teq r3, #10
  @Não:
    @4.2 Vai pra 3 -----------------------------------
    bne le_asterisco
  @Sim:
    @4.0 Zera o tamanho da chave inserida
    mov r9, #0
    @4.1 Vai pra 5 -----------------------------------
@5 Lê do Teclado Numérico ----------------------------
  ldr	r6,=LOCAL_STATUS_TECLADO
le_digito:
  ldr	r2,[r6]
  tst     r2,#BIT_MENOS_SIGNIFICATIVO
  beq     le_digito
  ldr	r1,=LOCAL_DADOS_TECLADO
  ldr	r3,[r1] @Passa o dígito

@6 Checa se é #: -------------------------------------
  teq r3, #11
  @Sim:
    @6.1 Vai pra 7 -----------------------------------
      beq descriptografa
  @Não:
    @6.2 Imprime * no console
      mov r0, #1
      ldr r1, =asterisco
      mov r2, #1
      mov r7, #4
      svc 0x55 @syscall. A mensagem está gravada na memória
    @6.3 Salva em algum lugar ------------------------
      ldr r1, =chave_inserida
      strb r3, [r1, r9] @Coloca o valor de r3 no array r1 na posição r9
      add r9, #1
    @6.4 Vai pra 5 -----------------------------------
      b le_digito
    ldr r10, =tamanho_chave_inserida
    str r9, [r10]
@7 Descriptografa a mensagem com a chave inserida ----
descriptografa:
@r4 vai ser o auxiliar pra trabalhar com os bytes
@r5 é o iterador
@r6 vai ter o vetor de inicialização ou o bloco posterior
@r8 chave tem a chave inserida
@r9 é o auxiliar que recebe a subtração entre r5 e r0 pra fazer o teste de condição
@r12 endereço da mensagem inserida
ldr r8, =chave_inserida @Endereço da chave inserida
ldr r8, [r8] @Aqui tem a chave que foi inserida
ldr r12, =mensagem_inserida @endereço da mensagem inserida
mov r5, #1020 @posição final do tamanho máximo da frase - 4 (certo é 1020)
mov r3, #1020 @iterador para gravar a mensagem descriptografada
ldr r2, =mensagem_descriptografada @posição inicial da mensagem descriptografada na memória
desblocacao: @inicio da descriptografia
  ldr r6, [r12, r5] @Sobrescreve o bloco da mensagem com o bloco criptografado
  sub r5, #4 @já pode subtrair 4 do iterador para pegar o bloco anterior criptografado (ou o vetor inicialização)
  ldr r4, [r12, r5] @r4 tem o bloco anterior ao que vai ser descriptografado
  eor r6, r4, r6 @XOR entre mensagem e bloco anterior (ou vetor de inicialização)
  eor r6, r6, r8 @XOR o resultado e a chave
  str r6, [r2, r3] @carrega o resultado para a frase descriptografada
  sub r3, #4 @soma 4 no iterador que escreve a frase descriptografada
  cmp r5, #0 @vê se ainda tem no mínimo 1 bloco para descriptografar
  bne desblocacao @se sim, volta para a desblocacao
ldr r5, =vetor_inicializacao @X bytes nulos na memória, onde X é o número de bytes da chave
ldr r5, [r5] @carrega X bytes nulos para r5, onde X é o número de bytes da chave
ldr r6, [r12, #0] @Vai pegar os 4 primeiros bytes da mensagem criptagrafada
eor r6, r5, r6 @XOR entre mensagem e bloco anterior (ou vetor de inicialização)
eor r6, r6, r8 @XOR o resultado e a chave
str r6, [r2, r3] @grava os 4 primeiros bytes da mensagem descriptografada na memória

@Imprime quebra de linha
mov     r0, #1
ldr     r1, =barra_ene
ldr     r2, =tam_barra
mov     r7, #4
svc     0x055



@8 Exibe o resultado no Console --------------------------
ldr r3, =tamanho_mensagem
ldr r3, [r3]
mov r0, #1
ldr r1, =mensagem_descriptografada
mov r2, r3
mov r7, #4
svc 0x55

@Imprime quebra de linha
mov     r0, #1
ldr     r1, =barra_ene
ldr     r2, =tam_barra
mov     r7, #4
svc     0x055

@9 Vai pra 3 -----------------------------------------
b tentativa_descriptografar
@-----------------------------------------------------
barra_ene: .ascii "\n"
tam_barra = . - barra_ene

mensagem_inserida: .skip 1024 @ Tamanho da mensagem: 1024 caracteres ASCII
mensagem_descriptografada:
  .skip 1024 @ Tamanho da mensagem: 1024 caracteres ASCII
chave_inserida:
  .skip 256 @Tamanho máximo da chave inserida é 64 bytes, ou seja, 64 dígitos
tamanho_mensagem:
  .skip 4 @Reserva 4 bytes (um inteiro) pra guardar o tamanho da mensagem em bytes
tamanho_chave_inserida:
  .skip 32 @Reserva um inteiro pra guardar o tamanho da chave inserida em bytes
chave:
  .byte 1, 2, 3, 4
tamanho_chave = . - chave @em bytes
vetor_inicializacao: .byte 4, 5, 2, 0
asterisco:
  .byte '*'
