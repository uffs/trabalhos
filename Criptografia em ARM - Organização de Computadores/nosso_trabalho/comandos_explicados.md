<table>
  <thead>
    <tr>
      <th>Comando</th>
      <th>Descrição</th>
      <th>Exemplo</th>
      <th>Explicação</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>add</td>
      <td>Adiciona</td>
      <td>add r5, r6</td>
      <td>r5 += r6</td>
    </tr>
    <tr>
      <td>b</td>
      <td>Branch</td>
      <td>beq _start</td>
      <td>Pula pro checkpoint _start se a instrução anterior tiver dado igual.</td>
    </tr>
    <tr>
      <td>beq</td>
      <td>Branch If Equal</td>
      <td>beq INICIO</td>
      <td>Pula pra "INICIO" no código caso a flag "zero" esteja com valor verdadeiro.</td>
    </tr>
    <tr>
      <td>mov</td>
      <td>Move</td>
      <td>mov r1, r2</td>
      <td>Copia do r2 pro r1. r1 = r2;</td>
    </tr>
    <tr>
      <td>s</td>
      <td>Sufixo S</td>
      <td>subs r4, r4, #1</td>
      <td>Atualiza as flags na operação. No exemplo dado, ele vai setar N se der negativo, Z se der 0 e outros...</td>
    </tr>
    <tr>
      <td>set</td>
      <td>Set</td>
      <td>set r5, 0x007</td>
      <td>r5 = 0x007</td>
    </tr>
    <tr>
      <td>tst</td>
      <td>Bitwise AND</td>
      <td>tst r1, #1</td>
      <td>Faz um bitwise AND entre dois valores e descarta o resultado (Mas seta as flags de acordo)</td>
    </tr>
    <tr>
      <td>.equ</td>
      <td>Definição de constante</td>
      <td>MAX .equ 1000</td>
      <td>#define MAX 1000</td>
    </tr>
    <tr>
      <td>.byte</td>
      <td>Reserva espaço para os valores dados</td>
      <td>palavra .byte 'PALAVRAO'</td>
      <td>char palavra[] = {'P', 'A', 'L', 'A', 'V', 'R', 'A'};</td>
    </tr>
    <tr>
      <td>.skip</td>
      <td>Reserva espaço do tamanho dado</td>
      <td>contador: .skip 16</td>
      <td>int contador[4];</td>
    </tr>
    <tr>
      <td>.word</td>
      <td>Reserva palavras</td>
      <td>contador .word 4</td>
      <td>int contador[4]</td>
    </tr>
  </tbody>
</table>

<h1>Comparação</h1>
![](./comparators.png)

<h1>Flags</h1>
<b>N</b>: Fica 1 quando o resultado de alguma operação der negativo. Está ligado ao bit 31 de um resultado.<br>
<b>Z</b>: Quando o resultado dá 0.
